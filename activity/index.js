const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


const fullName = ()=>{
	spanFullName.innerHTML = `${txtFirstName.value}  ${txtLastName.value}`
}

document.addEventListener('keyup',(event) =>{
	fullName();
})