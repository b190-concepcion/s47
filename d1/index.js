/* Using DOM

	before, we are using the following codes in terms of selecting elements inside the html file

*/
/*
document.getElementById('txt-first-name');
document.getElementByClassName('txt-last-name');
document.getElementByTagName('input');
*/

document.querySelector('#txt-first-name');

//Event Listeners
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


txtFirstName.addEventListener('keyup',(event) =>{
	spanFullName.innerHTML = txtFirstName.value;
})
/*
txtLastName.addEventListener('keyup',(event) =>{
	spanFullName.innerHTML =  txtLastName.value;
})
*/
txtFirstName.addEventListener('keypress',(event) =>{
	console.log(event.target)
	console.log(event.target.value)
})
